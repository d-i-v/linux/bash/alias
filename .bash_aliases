_myos="$(uname)"

alias prompt_cut='PS1="${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\n\$ "'

case $_myos in
  Linux)    alias ls='ls --color=auto'
            alias ll='ls -lah --color=auto'    
            alias l.='ls -ldh .* --color=auto';;
  Darwin)   alias ls='ls -Gh'
            alias ll='ls -lah -G'    
            alias l.='ls -ldh .* -G';;
  *) ;;
esac


alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias getapp='sudo apt-get install '
alias update='sudo apt-get update && sudo apt-get upgrade'

alias hist='history'
alias phist='history -c; history -w' #purge history
alias c='clear'

alias .1='cd ..'
alias .2='cd ../../'
alias .3='cd ../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../../'

alias devs='sudo blkid -o list -w /dev/null'
alias rb='sudo reboot'
alias getnameofip='nmblookup -A ' # Enter ip-adress
# alias disableipv6='sudo sh -c 'echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.d/01-disable-ipv6.conf''

# Refreshe Auflösung in vmware
alias setmon='xrandr -s 3;sleep 1;xrandr -s 2'

# TexLive 2022 Libary-Update
alias update_texlib22='sudo /usr/local/texlive/2022/bin/x86_64-linux/tlmgr update --all'


alias replace_alias='wget -O $HOME/.bash_aliases https://gitlab.com/d-i-v/linux/bash/alias/-/raw/main/.bash_aliases'


# Apps
#------
# Built Sphinx by Terminal
alias spc='f(){ rm -r "$1"/build/; sphinx-build -b html "$1"/source/ "$1"/build/; chromium "$1"/build/index.html;  unset -f f; }; f'

# setup esp32-environment (check directories)
alias get_idf='. $HOME/esp/esp-idf/export.sh'

# heic pics : sudo apt-get install libheif-examples
alias heic_to='function heicto(){ for file in *.heic; do heif-convert $file ${file/%.heic/."$1"}; done; }; heicto'

# Hardware
#----------
# Raspberry Pi 4
alias eeprom='sudo rpi-eeprom-update' #check status
alias eepromupdate='sudo rpi-eeprom-update -d -a'